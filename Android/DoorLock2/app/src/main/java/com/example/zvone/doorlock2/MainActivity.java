package com.example.zvone.doorlock2;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.os.Handler;
import android.os.Message;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";
    private final String deviceName ="HC-06";

    public boolean isConnected = false;
    public static final int STATE_LOCKED = 0;
    public static final int STATE_UNLOCKED = 1;
    public static int mDoorState = 0;
    public static DatabaseReference databaseReference;

    Button bttnConnect;
    Button bttnDoorLock;
    Button bttnDisconnect;
    TextView doorLockStatus;
    TextView connectionStatus;
    TextView logInfo;
    ImageView imageViewDoorLock;

    TextView securityInfo;

    public static String lastDateUpdate = null;

    BluetoothAdapter mBluetoothAdapter;
    BluetoothConnectionService mBluetoothConnectionService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bttnConnect = (Button) findViewById(R.id.bttnConnect);
        bttnDoorLock = (Button) findViewById(R.id.bttnDoorLock);
        bttnDisconnect = (Button) findViewById(R.id.bttnDisconnect);
        doorLockStatus = (TextView) findViewById(R.id.doorLockStatus);
        connectionStatus = (TextView) findViewById(R.id.connectionStatus);
        imageViewDoorLock = (ImageView) findViewById(R.id.imageViewDoorLock);
        logInfo = (TextView) findViewById(R.id.logInfo);
        securityInfo=(TextView) findViewById(R.id.securityInfo);

        logInfo.setVisibility(View.INVISIBLE);

        imageViewDoorLock.setBackgroundResource(R.drawable.locked_pic);

        mBluetoothConnectionService = new BluetoothConnectionService(this, mHandler);

        databaseReference = FirebaseDatabase.getInstance().getReference("LogDoorLock");

        bttnDisconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnectBT();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        changeDoorListener();

    }

    @Override
    protected void onResume() {
        super.onResume();
        changeDoorListener();
    }

    private void changeDoorListener() {

        SharedPreferences preferences = getSharedPreferences("lastUpdate", Context.MODE_PRIVATE);
        final String lastUpdate =preferences.getString("lastUpdate","");

        databaseReference.orderByKey().limitToLast(1).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Door state listener: " + dataSnapshot.getKey());
                LogDoorLock oneEvent = dataSnapshot.getValue(LogDoorLock.class);

                if(oneEvent != null) {
                    if(!lastUpdate.equals(oneEvent.getDate())) {
                        String deviceName = oneEvent.getDeviceName();
                        String doorState = oneEvent.getDoorStatus();
                        securityInfo.setText("Door was " + doorState + " by " + deviceName);
                        logInfo.setVisibility(View.VISIBLE);
                        lastDateUpdate = oneEvent.getDate();
                    }

                    switch(oneEvent.getDoorStatus().toString()){
                        case "Unlocked":
                            setStateUnlocked();
                            break;
                        case "Locked":
                            setStateLocked();
                            break;
                    }
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void saveTimeToPreference(String time){
        SharedPreferences preferences = getSharedPreferences("lastUpdate", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lastUpdate", time);
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.menu_logs:
                startLogActivity();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void connectService(View view) {

        Log.d(TAG, "Connecting to Bluetooth device started!");

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(mBluetoothAdapter == null) {
            Log.d(TAG, "Device does not support Bluetooth");
            Toast.makeText(this, "Device does not support Bluetooth", Toast.LENGTH_SHORT).show();
            isConnected = false;
            return;
        }

        if(mBluetoothAdapter.isEnabled()) {
            startBTConnection();
        }
        else {
            Intent enableBtIntetnt = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBtIntetnt);
            Log.d(TAG, "Enabling Bluetooth");
            Toast.makeText(this, "Enabling Bluetooth", Toast.LENGTH_SHORT).show();
            startBTConnection();
        }
    }

    public void startBTConnection() {

        mBluetoothConnectionService.start();
        mBluetoothConnectionService.connectDevice(deviceName);

    }

    public void changeDoorLockState(View v) {

        if(!isConnected) {
            Log.d(TAG, "Bluetooth is not enabled or device is not connected");
            Toast.makeText(this, "Bluetooth is not enabled or device is not connected",
                            Toast.LENGTH_SHORT).show();
            return;
        }

        switch(mDoorState){
            case STATE_LOCKED:
                String open = "OPEN";
                mBluetoothConnectionService.sendMessage(open);
                setStateUnlocked();
                break;
            case STATE_UNLOCKED:
                String close = "LOCK";
                mBluetoothConnectionService.sendMessage(close);
                setStateLocked();
                break;
        }

        saveToDatabase(mDoorState);

        Toast.makeText(this, "Successfully changed door state", Toast.LENGTH_SHORT).show();
    }

    public void setStateLocked() {
        doorLockStatus.setText("DOOR LOCKED");
        doorLockStatus.setTextColor(Color.parseColor("#FF0000"));
        bttnDoorLock.setText("OPEN");
        imageViewDoorLock.setBackgroundResource(R.drawable.locked_pic);
        mDoorState = STATE_LOCKED;
    }

    public void setStateUnlocked() {
        mDoorState = STATE_UNLOCKED;
        doorLockStatus.setText("DOOR OPEN");
        doorLockStatus.setTextColor(Color.parseColor("#00FF00"));
        bttnDoorLock.setText("CLOSE");
        imageViewDoorLock.setBackgroundResource(R.drawable.unlocked_pic);
    }

    public void disconnectBT() {

        String disconnect = "DISCONNECT";
        mBluetoothConnectionService.sendMessage(disconnect);

        waitTilSignalIsSend(2000);

        isConnected = false;
        mDoorState = STATE_LOCKED;
        doorLockStatus.setText("DOOR CLOSED");
        doorLockStatus.setTextColor(Color.parseColor("#FF0000"));
        bttnDoorLock.setText("OPEN");
        connectionStatus.setText("Connection status: Disconnected");
        imageViewDoorLock.setBackgroundResource(R.drawable.locked_pic);
        mBluetoothConnectionService.stop();

    }

    // It is used to give enough time so Arduino can receive signal
    // before threads are stopped
    public void waitTilSignalIsSend(int milis) {
        SystemClock.sleep(milis);
    }

    private void startLogActivity() {

        if(lastDateUpdate != null){
            saveTimeToPreference(lastDateUpdate);
        }
        Intent intent = new Intent(this, LogDoorActivity.class);
        startActivity(intent);
        logInfo.setVisibility(View.INVISIBLE);
        securityInfo.setText("");

    }

    /* This function save door state to Firebase service */
    private void saveToDatabase(int state) {

        Log.d(TAG, "saveToDatabase()");

        String deviceName = getDeviceName();
        Date currentDate = new Date();
        LogDoorLock log;
        String doorState;
        String date;
        String timeForPreference;

        SimpleDateFormat formatter = new SimpleDateFormat("EEEE, d. MMMM yyyy, 'vrijeme:' HH:mm:ss");
        SimpleDateFormat preferenceFormater = new SimpleDateFormat("HH:mm:ss");
        date = formatter.format(currentDate);
        timeForPreference = preferenceFormater.format(currentDate);


        if(state == STATE_LOCKED) {
            doorState = new String("Locked");
        }
        else {
            doorState = new String("Unlocked");
        }

        String logId = databaseReference.push().getKey();

        log = new LogDoorLock(logId, deviceName, doorState, date, timeForPreference);

        databaseReference.child(logId).setValue(log);

        Log.d(TAG, "Log object successfully added to database");

        saveTimeToPreference(date);

    }

    /* Returns the consumer friendly device name */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }


    // message Handler for updating UI
    private final Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothConnectionService.MESSAGE_STATE_CHANGE:
                    Log.d(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);

                    if(msg.arg1 == BluetoothConnectionService.STATE_CONNECTED){
                        isConnected = true;
                        connectionStatus.setText("Connection status: Connected");
                    }
                    else if (msg.arg1 == BluetoothConnectionService.STATE_CONNECTING){
                        connectionStatus.setText("Connecting...");
                    }
                    break;
                case BluetoothConnectionService.MESSAGE_WRITE:
                    Log.d(TAG, "MESSAGE_WRITE ");
                    
                    break;
                case BluetoothConnectionService.MESSAGE_READ:
                    Log.d(TAG, "MESSAGE_READ");
                    break;
                case BluetoothConnectionService.MESSAGE_DEVICE_NAME:
                    Log.d(TAG, "MESSAGE_DEVICE_NAME ");
                    break;
                case BluetoothConnectionService.MESSAGE_TOAST:
                    Log.d(TAG, "MESSAGE_TOAST "+msg.getData());
                    String message = msg.getData().toString();
                    if(message.contains("failed") || message.contains("lost")){
                        Log.d(TAG, "Connection failed or lost: " + message);
                        isConnected = false;
                        connectionStatus.setText("Connection status: Disonnected");
                    }
                    break;
            }
        }
    };
}
