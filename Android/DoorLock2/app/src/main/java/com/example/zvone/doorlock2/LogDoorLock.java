package com.example.zvone.doorlock2;


public class LogDoorLock {

    private String logId;
    private String deviceName;
    private String doorStatus;
    private String date;
    private String timeForPreference;

    public LogDoorLock() {};

    public LogDoorLock(String logId, String deviceName, String doorStatus, String date, String timeForPreference) {
        this.logId = logId;
        this.deviceName = deviceName;
        this.doorStatus = doorStatus;
        this.date = date;
        this.timeForPreference = timeForPreference;
    }

    public String getLogId() {
        return logId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDoorStatus() {
        return doorStatus;
    }

    public String getDate() {
        return date;
    }

    public String getTimeForPreference() {
        return timeForPreference;
    }
}
