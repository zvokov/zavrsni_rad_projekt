package com.example.zvone.doorlock2;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class ListViewAdapter extends ArrayAdapter<LogDoorLock> {

    private final String TAG  = "DOORLOCKLOG";
    private Activity context;
    private List<LogDoorLock> logDoorLockList;

    public ListViewAdapter(Activity context, List<LogDoorLock> logDoorLockList) {
        super(context, R.layout.list_view, logDoorLockList);
        this.context = context;
        this.logDoorLockList = logDoorLockList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.list_view, null, true);

        TextView textViewDoorState = (TextView) listViewItem.findViewById(R.id.textViewDoorState);
        TextView textViewAdditionalInfo = (TextView) listViewItem.findViewById(R.id.textViewAdditionalInfo);
        TextView textViewDateInfo = (TextView) listViewItem.findViewById(R.id.textViewDateInfo);
        LogDoorLock log = logDoorLockList.get(position);

        textViewDoorState.setText("Door "+log.getDoorStatus());
        textViewAdditionalInfo.setText("By " + log.getDeviceName());
        textViewDateInfo.setText("Date: " + log.getDate());

        return listViewItem;
    }
}
