package com.example.zvone.doorlock2;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ShowableListMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LogDoorActivity extends AppCompatActivity  {

    ListView listView;

    List<LogDoorLock> logDoorLockList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_door);

        listView = (ListView) findViewById(R.id.listViewId);
        logDoorLockList = new ArrayList<>();

        // Add back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.door_lock_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case android.R.id.home:
                this.finish();
                break;

            case R.id.menu_remove:
                showCustomDialog();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void showCustomDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Clear logs!")
                .setMessage("Remove logs from database?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.databaseReference.removeValue();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create().show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        MainActivity.databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                logDoorLockList.clear();

                for(DataSnapshot logsnapShot : dataSnapshot.getChildren()){
                    LogDoorLock log = logsnapShot.getValue(LogDoorLock.class);
                    logDoorLockList.add(log);
                }

                Collections.reverse(logDoorLockList);
                ListViewAdapter adapter = new ListViewAdapter(LogDoorActivity.this, logDoorLockList);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
