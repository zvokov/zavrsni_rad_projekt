package com.example.zvone.doorlock2;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;


public class BluetoothConnectionService {

    private static final String TAG = "BluetoothService";

    // Unique UUID
    private static final UUID MY_UUID = UUID.fromString("0001101-0000-1000-8000-00805F9B34FB");

    private static final String NAME = "BluetoothChat";

    // Constants that indicates the current connection state
    public static final int STATE_NONE = 0;
    public static final int STATE_LISTEN = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_CONNECTED = 3;

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    // Member fields
    private final BluetoothAdapter mBluetoothAdapter;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private final Handler mHandler;
    public static int mState;

    private static Context mContext;
    ProgressDialog mProgressDialog;

    public BluetoothConnectionService(Context context, Handler handler) {

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        for(BluetoothDevice bd : mBluetoothAdapter.getBondedDevices()) {
            Log.d(TAG, "Bounded device " + bd);
        }

        mState = STATE_NONE;
        mContext = context;
        mHandler = handler;
    }

    /* Set the current state of the chat connection */
    private synchronized void setState(int state) {

        Log.d(TAG, "setState() " + mState + " -> " + state);

        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(MESSAGE_STATE_CHANGE, state, -1)
                .sendToTarget();

        mState = state;
    }

    /* Return the current connection state */
    public synchronized int getState() {
        return mState;
    }

    /* Start the chat service. Specifically start AcceptThread to begin a
    * session in listening (server) mode. Called by the Activity onResume()
    */
    public synchronized void start() {

        Log.d(TAG, "start()");

        // Cancel any thread attempting to make a connection
        if(mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if(mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_LISTEN);

    }

    /* Start the ConnectThread to initiate a connection to a remote device
    * @param device : The BluetoothDevice to connect
    */
    private  synchronized void connect(BluetoothDevice device) {

        Log.d(TAG, "connect() to: " + device.getName());

        // Cancel any thread attempting to make a connection
        if(mState == STATE_CONNECTING) {
            if(mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if(mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mProgressDialog = ProgressDialog.show(mContext, "Connecting to Bluetooth", "Please wait..", true);
        // Start the thread to connect with the given device
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();

        setState(STATE_CONNECTING);

    }

    /* Start the ConnectedThread to begin managing a Bluetooth connection
    * @param socket : The BluetoothSocket on which the connection was made
    * @param device : The BluetoothDevice that has been connected
    */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device,
                                       final String socketType) {

        Log.d(TAG, "connected(), Socket Type: " + socketType);

        // Cancel the thread that completed the connection
        if(mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        // Cancel any thread currently running a connection
        if(mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to manage the connection and perform transmission
        mConnectedThread = new ConnectedThread(socket, socketType);
        mConnectedThread.start();

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString("Connected", device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);

    }

    /* Stop all threads */
    public synchronized void stop() {

        Log.d(TAG, "stop() all threads");

        if(mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if(mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_NONE);

    }

    /* Write to the ConnectedThread in an unsynchronized manner
    * @param out : The bytes to write
    */
    public void write(byte[] out) {

        // Create temporary object
        ConnectedThread tmp;

        synchronized (this) {
            if(mState != STATE_CONNECTED) return;
            tmp = mConnectedThread;
        }

        // Perform the write unsynchronized
        tmp.write(out);
    }

    /* Indicate that the connection attempt failed and notify the UI */
    private void connectionFailed() {
        try{
            mProgressDialog.dismiss();
        }catch(NullPointerException e){
            e.printStackTrace();
        }

        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString("Toast", "Connection failed, unable to connect device");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        Log.d(TAG, "Connection attempt failed");
        BluetoothConnectionService.this.start();

    }

    /* Indicate that the connection was lost */
    private void connectionLost() {

        // Send a failure message back to the Activity
        Message msg = mHandler.obtainMessage(MESSAGE_TOAST);
        Bundle bundle = new Bundle();
        bundle.putString("Toast", "Device connection was lost");
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        Log.d(TAG, "Device connection was lost");
        BluetoothConnectionService.this.start();

    }

    /* This thread runs while attempting to make an outgoing connection with a device.
    * It runs straight through; the connection either succeeds or fails. */
    private class ConnectThread extends Thread {

        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private String mSocketType;

        public ConnectThread(BluetoothDevice device) {

            mmDevice = device;
            BluetoothSocket tmp = null;

            // Get a BluetoothSocket for a connection with the given BluetoothDevice
            try {
                tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
            }catch(IOException e){
                Log.e(TAG, "Socket Type: " + mSocketType + " create() failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectThread SocketType: " + mSocketType);
            setName("ConnectThread" + mSocketType);

            // Always cancel discovery because it will slow down a connection
            mBluetoothAdapter.cancelDiscovery();

            // Make a connection to the BluetoothSocket
            try{
                // This is a blocking call and will only return a successful connection
                // or an exception
                mmSocket.connect();
            }catch(IOException e) {
                Log.e(TAG, "Unable to connect socket", e);

                // Close the socket
                try{
                    mmSocket.close();
                }catch(IOException e2) {
                    Log.e(TAG, "unable to close() " + mSocketType
                    + " socket during connection failure", e2);
                }
                connectionFailed();
                return;
            }

            // Reset the ConnectThread because we're done
            synchronized (BluetoothConnectionService.this) {
                mConnectThread = null;
            }

            // Start the connected thread
            connected(mmSocket, mmDevice, mSocketType);
        }

        public void cancel() {
            try{
                mmSocket.close();
            }catch (IOException e) {
                Log.e(TAG, "close() of connect " + mSocketType + "socket failed", e);
            }
        }
    }

    /* This thread runs during a connection with a remote device. It handles all
    * incoming and outgoing transmissions.*/
    private class ConnectedThread extends Thread {

        private final BluetoothSocket mmSocket;
        private final InputStream mmInputStream;
        private final OutputStream mmOutputStream;

        public ConnectedThread(BluetoothSocket socket, String socketType){

            Log.d(TAG, "create ConnectedThread: " + socketType);
            mmSocket = socket;
            InputStream tmpInput = null;
            OutputStream tmpOutput = null;

            try{
                mProgressDialog.dismiss();
            }catch(NullPointerException e){
                e.printStackTrace();
            }

            // Get the BluetoothSocket input and output streams
            try{
                tmpInput = socket.getInputStream();
                tmpOutput = socket.getOutputStream();
            }catch (IOException e){
                Log.e(TAG, "tmp sockets not created", e);
            }

            mmInputStream = tmpInput;
            mmOutputStream = tmpOutput;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread");
            byte[] buffer = new byte[1024];
            int bytes;

            // Keep listening to the InputStream while connected
            while(true){
                try{
                    // Read from the InputStream
                    bytes = mmInputStream.read(buffer);
                    String incommingMessage = new String(buffer, 0 , bytes);
                    Log.d(TAG, "message buffer " + incommingMessage);
                    // Send the obtained bytes to the UI Activity
					mHandler.obtainMessage(MESSAGE_READ, bytes,
							-1, incommingMessage).sendToTarget();
                }catch (IOException e) {
                    Log.e(TAG, "disconnected", e);
                    connectionLost();

                    // Start the service over to restart listening mode
                    BluetoothConnectionService.this.start();
                    break;
                }
            }
        }

        /* Write to the connected OutStream.
        * @param buffer : The bytes to write*/
        public void write(byte[] buffer) {
            String text = new String(buffer, Charset.defaultCharset());
            Log.d(TAG, "OUTPUT STREAM : " + text);
            try{
                mmOutputStream.write(buffer);
                Log.d(TAG, "successful writing : " + text);
                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(MESSAGE_WRITE, -1, -1,
                        buffer).sendToTarget();
            }catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try{
                mmSocket.close();
            }catch(IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }

    public void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if(this.getState() != BluetoothConnectionService.STATE_CONNECTED) {
            Log.w(TAG, "Bluetooth is not connected");
            return;
        }

        // Check that there is actually something to send
        if(message.length() > 0) {
            byte[] send = message.getBytes(Charset.defaultCharset());
            Log.d(TAG, "message to be send " + send + " message :" + message);
            this.write(send);
        }
    }


    public void connectDevice(String deviceName) {

        // Get the device MAC address
        String address = null;
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        for(BluetoothDevice bd : bluetoothAdapter.getBondedDevices()) {
            if(bd.getName().equals(deviceName)){
                address = bd.getAddress();
            }
        }

        try{
            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
            this.connect(device);
            Log.d(TAG, "Trying to connect to " + address);
        } catch (Exception e) {
            Log.e("Unable to connect to " + address, e.getMessage());

        }

    }
}
