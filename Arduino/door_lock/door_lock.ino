
// deklaracija i inicijalizacija izlaznih pinova i varijabli
int ledYellowPin = 8;
int ledGreenPin = 9;
int ledRedPin = 10;

int relayPin = 3;
const byte BTpin = 4;

String input;
int connected = 0;


void setup() {

  Serial.begin(9600);
  
  pinMode(ledYellowPin, OUTPUT);
  pinMode(ledRedPin, OUTPUT);
  pinMode(ledGreenPin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  pinMode(BTpin, INPUT);

  digitalWrite(ledRedPin, HIGH);
  digitalWrite(ledYellowPin,LOW);
  digitalWrite(ledGreenPin, LOW);
  digitalWrite(relayPin, LOW);
}



void loop() {

  if(digitalRead(BTpin) == HIGH){
    digitalWrite(ledYellowPin, HIGH);
    connected = 1;
    Serial.println("Device connected!");
  }
  else {
    digitalWrite(ledYellowPin, LOW);
    connected = 0;
  }
  
 if(Serial.available() > 0) {
   input = Serial.readString();
   Serial.print("Received: ");
   Serial.println(input);

  if (input == "OPEN" && connected == 1){
    digitalWrite(ledGreenPin, HIGH);
    digitalWrite(relejPin, HIGH);
    digitalWrite(ledRedPin, LOW);
    input = "";
    Serial.println("Door open!");
  } 

  if (input == "LOCK" && connected == 1) {
    digitalWrite(ledRedPin, HIGH);
    digitalWrite(relayPin, LOW);
    digitalWrite(ledGreenPin, LOW);
    input = "";
    Serial.println("Door locked!");
  }
  
  if (input == "DISCONNECT") {
    digitalWrite(ledYellowPin,LOW);
    digitalWrite(ledGreenPin, LOW);
    digitalWrite(ledRedPin, HIGH);
    digitalWrite(relayPin, LOW);
    connected = 0;
    input = "";
    Serial.println("Device disconnected!");
  }
 }

}



